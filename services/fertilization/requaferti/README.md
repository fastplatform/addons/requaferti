# `addons/requaferti/fertilization`: Requaferti fertilization service for Wallonia

This service exposes [Requaferti](https://www.requaferti.requasud.be/) fertilization recommendation to the Wallonian users of FaST and is based on a web service exposed by [Requasud](https://www.requasud.be/). The service itself does not do any fertilization computation (i.e. it contains no agronomic logic): all the calculations are done in the Requasud/Requaferti web service and this connector only acts as a proxy.

## Architecture

It is composed of a GraphQL server implemented using the [Graphene](https://graphene-python.org/) framework + [FastAPI](https://fastapi.tiangolo.com/)/[Starlette](https://www.starlette.io/) and served behind a [uvicorn](https://www.uvicorn.org/) HTTP server.

The service exposes a single `/graphql` endpoint and depends on the availability of the Requaferti API, as mentioned above.

```plantuml

agent "Farmer app" as farmer

component "FaST API Gateway" as api #line.dashed
component "Web server" as web {
    file "Default\nsoil data"
    storage "In-memory\ncache"
}
cloud "Requaferti API" as db

farmer --> api: /graphql
api --> web: /graphql
web --> db

```

#### Service startup

When the service starts, it loads and indexes into memory the following data:
- the [shapefile of all postal codes](app/lib/codes_postaux), with a geographical index
- the [shapefile of all agricultural regions](app/lib/regions_agricoles/), with a geopgraphical index
- the [shapefile of the gravel content of the soil](app/lib/sol/charge_caillouteuse/), with a geographical index
- the [default soil values per postal code](app/lib/parametres_sol_par_code_postal.csv)
- the [default soil values per agricultural region](app/lib/sol/parametres_sol_par_region_agricole.csv)
- the [default soil values for the whole Wallonia](app/lib/sol/parametres_sol_par_defaut.csv)

The service therefore take a few seconds to start up, due to this indexing.

## GraphQL server

The service exposes a GraphQL server on the `/graphql` endpoint. The following (high-level) GraphQL schema is exposed:
```graphql
query {
    requaferti__config
    requaferti__postal_code
    requaferti__agricultural_region
    requaferti__soil
    requaferti__compute_n
    requaferti__generate_results_pdf
}

# The service does not expose any mutation or subscription.
```

The schema is self documenting and we encourage the reader to introspect it to learn more about each node and its associated fields and relations. This can done using the [GraphiQL](https://github.com/graphql/graphiql) or [Altair](https://altair.sirmuel.design/) tools, for example:

<div align="center">
    <img src="docs/img/graphql-schema.png" width="420">
</div>

Several [example queries](app/tests/graphql/) are provided for each node, as part of the test suite.

#### Querying the configuration

The service exposes the Requaferti configuration on the `requaferti__config` node. The configuration is retrieved from the Requasud endpoints and cached (in-memory) for 1 hour. It is returned to the GraphQL client as-is.

#### Querying soil estimates for a given parcel

The service computes an estimate of the soil properties for a given parcel, based on its geometry (geographical extent):
1. determine the postal code of the parcel (max intersection with postal code geometries)
2. determine the agricultural region of the parcel (max intersection with agricultural region geometries)
3. for each soil property:
    - if the property is passed as an argument (i.e. it was specified by the user), use it
    - if not, use the default value for the postal code if present
    - if not, use the default value for the agricultural region if present
    - if not, use the default value for the whole Wallonia if present
    - if not, return an empty value
    - \* note that there is a special case for the gravel content (_charge caillouteuse_), which is computed from the gravel content shapefile
4. return the soil properties

#### Querying fertilization recommendation

The service acts as a simple proxy and each request is reformatted and passed on to the Requaferti web service if not available in the local in-memory cache. The response is then reformatted back and returned to the GraphQL client.

### Environment variables

- `REQUAFERTI_USERNAME`: username to authenticate into the Requaferti web service
- `REQUAFERTI_PASSWORD`: password to authenticate into the Requaferti web service
- `REQUAFERTI_DEFAULT_TIMEOUT`: timeout of calls to the Requaferti API (default to `10` seconds)
- `REQUAFERTI_COMPUTE_N_URL`: URL of the Requaferti nitrogen recommendation endpoint (defaults to `https://www.requaferti.requasud.be/requaferti_ws`)
- `REQUAFERTI_COMPUTE_N_CACHE_MAX_SIZE`: maximum number of nitrogen recommendation API calls that are cached in memory (defaults to `10000`)
- `REQUAFERTI_COMPUTE_N_CACHE_TTL`: time-to-live of the nitrogen recommendation cache entries (defaults to `300` seconds)

## Development 

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the server with auto-reload enabled:
```bash
make start
```

The service is now available at `http://localhost:7777/graphql`.
