import logging
import uvicorn

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import graphene
from starlette_graphene3 import GraphQLApp

from app.settings import config
from app.tracing import Tracing
from app.api.query import Query
from app.lib.requaferti import requaferti_client


# FastAPI
app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# GraphQL root
app.add_route(
    "/graphql",
    GraphQLApp(
        schema=graphene.Schema(query=Query, auto_camelcase=False)
    ),
)

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():
    logger.debug(config)
    requaferti_client.create_http_client()
    requaferti_client.load_geographic_data()


@app.on_event("shutdown")
async def shutdown():
    await requaferti_client.close_http_client()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7777)