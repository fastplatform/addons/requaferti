import logging
import base64

import requests
import graphene
from graphene_pydantic_updated import PydanticObjectType, PydanticInputObjectType
from graphene.types.generic import GenericScalar
from opentelemetry import trace
import shapely.ops
import shapely.geometry

from app.settings import config
from app.api.errors import GraphQLServiceError
from app.lib.requaferti_types import (
    RequafertiComputeNParams,
    RequafertiComputeNResult,
    RequafertiGetSolResultType,
    PDFGeneratorInputType,
    PDFGeneratorOutputType,
)
from app.lib.requaferti import requaferti_client
from app.lib.gis import Projection


logger = logging.getLogger(__name__)


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class RequafertiSoilResponseType(PydanticObjectType):
    class Meta:
        model = RequafertiGetSolResultType
        name = "requaferti__soil_response"


class RequafertiComputeNParamsType(PydanticInputObjectType):
    class Meta:
        model = RequafertiComputeNParams
        name = "requaferti__compute_n_params"


class RequafertiComputeNResultType(PydanticObjectType):
    class Meta:
        model = RequafertiComputeNResult
        name = "requaferti__compute_n_result"


class Query(graphene.ObjectType):

    requaferti__healthz = graphene.String(default_value="ok")
    requaferti__config = GenericScalar(
        description="Requaferti configuration, as received from Requasud"
    )  # a dict
    requaferti__postal_code = graphene.Field(
        graphene.String,
        description="Postal code",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
    )
    requaferti__agricultural_region = graphene.Field(
        graphene.String,
        description="Agricultural region",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
    )
    requaferti__soil = graphene.Field(
        RequafertiSoilResponseType,
        description="Soil properties",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
        couche_arable=graphene.Argument(graphene.Float, required=False),
        argile=graphene.Argument(graphene.Float, required=False),
        calcaire=graphene.Argument(graphene.Float, required=False),
        carbone=graphene.Argument(graphene.Float, required=False),
        nnh4_profil=graphene.Argument(graphene.Float, required=False),
        nno3_profil=graphene.Argument(graphene.Float, required=False),
        densite=graphene.Argument(graphene.Float, required=False),
        cailloux=graphene.Argument(graphene.Float, required=False),
        rapport_c_n=graphene.Argument(graphene.Float, required=False),
        mo=graphene.Argument(graphene.Float, required=False),
        nhumus=graphene.Argument(graphene.Float, required=False),
    )
    requaferti__compute_n = graphene.Field(
        RequafertiComputeNResultType,
        description="Compute nitrogen recommendation",
        params=graphene.Argument(RequafertiComputeNParamsType, required=True),
    )

    requaferti__generate_results_pdf = graphene.NonNull(
        PDFGeneratorOutputType,
        description="Generate the pdf file for the Requaferti results",
        input=graphene.Argument(PDFGeneratorInputType, required=True),
    )

    def resolve_requaferti__config(self, info):
        try:
            return requaferti_client.get_config()
        except Exception as exc:
            logger.exception("Failed to get the configuration")
            raise GraphQLServiceError() from exc

    def resolve_requaferti__postal_code(self, info, geometry):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__

            code_postal = requaferti_client.get_code_postal(geometry)
            return code_postal
        except Exception as exc:
            logger.exception("Failed to compute code postal")
            raise GraphQLServiceError() from exc

    def resolve_requaferti__agricultural_region(self, info, geometry):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__

            region_agricole = requaferti_client.get_region_agricole(geometry)
            return region_agricole
        except Exception as exc:
            logger.exception("Failed to compute region agricole")
            raise GraphQLServiceError() from exc

    def resolve_requaferti__soil(self, info, geometry, **kwargs):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__

            sol = requaferti_client.get_sol(geometry, **kwargs)
            return sol
        except Exception as exc:
            logger.exception("Failed to get sol")
            raise GraphQLServiceError() from exc

    async def resolve_requaferti__compute_n(self, info, params):
        try:
            result = await requaferti_client.compute_n(
                params=RequafertiComputeNParams(**params)
            )
        except Exception as exc:
            logger.exception("Failed to compute nitrogen")
            raise GraphQLServiceError() from exc

        return result

    def resolve_requaferti__generate_results_pdf(self, info, input):

        inputs = input["inputs"]

        try:
            # Get algorithm logo as static image
            with open(config.APP_DIR / "static/logo_requaferti.png", "rb") as f:
                logo = f.read()
                inputs["algorithm_logo_base64"] = (
                    "data:image/png;base64," + base64.b64encode(logo).decode()
                )
        except Exception as exc:
            logger.exception("Failed to read algorithm logo")
            raise GraphQLServiceError() from exc

        try:
            language = info.context["request"].headers["x-hasura-language"]
            print('language = ' + language)
        except:
            logger.exception("Could not retrieve language from info, will use default template")
            language = 'fr'

        with tracer().start_as_current_span("generate_pdf"):
            try:
                template_file = "base_de.html" if language == 'de' else 'base.html'
                template_path =  "templates/pdf/" + template_file
                with open(config.APP_DIR / template_path, "r") as f:
                    template = f.read()
                response = requests.post(
                    config.PDF_GENERATOR_ENDPOINT,
                    headers={},
                    json={"template": template, "data": inputs},
                )
                response.raise_for_status()

                return PDFGeneratorOutputType(pdf=response.content)

            except Exception as exc:
                logger.exception("Failed to build pdf")
                raise GraphQLServiceError() from exc
