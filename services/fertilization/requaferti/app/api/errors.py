from graphql import GraphQLError


class GraphQLServiceError(GraphQLError):
    message = "SERVICE_ERROR"

    def __init__(self, *args, **kwargs):
        super().__init__(GraphQLServiceError.message, *args, **kwargs)
