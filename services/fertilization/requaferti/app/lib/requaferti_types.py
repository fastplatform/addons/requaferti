from datetime import date
from typing import Optional

import logging
import graphene

from pydantic import ValidationError, root_validator
from pydantic import BaseModel, Field


logger = logging.getLogger(__name__)


def check_all_or_none(values, fields):
    values_has_fields = [f is not None in values for f in fields]
    if not all(values_has_fields) and any(values_has_fields):
        raise ValidationError(
            f"All the following fields should be filled (or none): {fields}"
        )


class RequafertiComputeNParams(BaseModel):

    # info de l'echantillon
    ech_date: date  # date échantillon / example: "2017-06-20"
    ech_num_ana: str  # numéro analyse / example: "test ws"
    ech_cp: Optional[int]  # Code postal / example: 5030
    ech_geoid: Optional[str]  # Spécifique pour certains clients
    ech_x: Optional[float]  # Easting (X)
    ech_y: Optional[float]  # Northing (Y)

    sol_cond_def_autres_yn: Optional[bool]  # Conditions défavorable du Sol / example: 1

    # arriere effet prairie
    age_prairie: Optional[int]  # Age prairie / example: 2
    period_destruct: Optional[int]  # Période destruction / example: 1
    rang_culture: Optional[int]  # Rang de la culture / example: 2

    # arriere effet luzerne
    age_luzerne: Optional[int]  # Age de la luzerne
    rang_luzerne: Optional[int]  # Rang de la luzerne

    # info sur la culture
    type_cult_xid: int  # Type de culture / example: 41
    cult_obj_rend: Optional[float]  # Objectif de rendement (q/ha) / example: 22
    cult_exportation: Optional[float]  # Exportation (kg N/ha) / example: 260
    stade_vege_xid: Optional[int]  # Stade vegetation
    placette: Optional[int]  # Placette
    recouv_xid: Optional[int]  # Recouvrement
    haut_epi_xid: Optional[int]  # Hauteur épinard

    # effet cipan
    cult_inter_cipan: Optional[int]  # Culture intermediaire
    type_prod_cipan: Optional[int]  # Type de production
    eff_cip_yn: Optional[bool]  # Prise en compte par ailleurs

    # effet precedent
    type_cult_eff_prec_id: Optional[int]  # Précedent cultural / example: 50
    dest_part_aer_id: Optional[int]  # Destination de la partie aérienne / example: 6
    cult_eff_prec_yn: Optional[bool]  # Prise en compte par ailleurs / example: 1

    # apport organique

    frequence_xid: int  # Fréquence / example: 3

    # apport organique 1
    cat_anim_app1: Optional[int]  # Catégorie animal / example: 1
    typ_mo_app1: Optional[int]  # Type engrais / example: 1
    quantite_eff_app1: Optional[float]  # Quantité (t MF) / example: 12
    teneur_n_app1: Optional[float]  # Teneur N (kg N/t MF) / example: 3.5
    date_app_app1: Optional[date]  # Date apport / example: "2017-05-03"
    cemi_yn_app1: Optional[bool]  # Prise en compte cemi / Example: 0

    # apport organique 2
    cat_anim_app2: Optional[int]  # Catégorie animal
    typ_mo_app2: Optional[int]  # Type engrais
    quantite_eff_app2: Optional[float]  # Quantité (t MF)
    teneur_n_app2: Optional[float]  # Teneur N (kg N/t MF)
    date_app_app2: Optional[date]  # Date apport
    cemi_yn_app2: Optional[bool]  # Prise en compte cemi

    # apport organique 3
    cat_anim_app3: Optional[int]  # Catégorie animal
    typ_mo_app3: Optional[int]  # Type engrais
    quantite_eff_app3: Optional[float]  # Quantité (t MF)
    teneur_n_app3: Optional[float]  # Teneur N (kg N/t MF)
    date_app_app3: Optional[date]  # Date apport
    cemi_yn_app3: Optional[bool]  # Prise en compte cemi

    # info sol
    sol_nno3_profil: float  # Exemple: 8
    sol_nnh4_profil: Optional[float]  # Exemple: 0
    sol_cailloux: Optional[float]  # Exemple: 0
    sol_densite: Optional[float]  # Exemple: 1.35
    sol_couche_arable: Optional[float]  # Exemple:: 30
    sol_temp_air: Optional[float]  # Exemple: 10.6
    sol_argile: float  # Exemple: 5
    sol_calcaire: Optional[float]  # Exemple: 8
    sol_mo: Optional[float]  # Exemple: 6
    sol_carbone: Optional[float]  # Exemple: 3
    sol_km: Optional[float]  # Exemple: 0.033
    sol_fact_conv: Optional[float]  # Exemple: 2
    sol_nhumus: Optional[float]  # Exemple: 2.7777777777778
    sol_reliquat_n: Optional[float]  # Exemple: 20
    sol_pot_mineral: Optional[float]  # ?

    @root_validator
    @classmethod
    def check_ech(cls, values):
        if values["ech_cp"] is None and (
            values["ech_x"] is None or values["ech_y"] is None
        ):
            raise ValidationError("ech_cp or ech_x and ech_y must be provided")
        return values

    @root_validator
    @classmethod
    def check_effet_prec_or_cipan(cls, values):
        if (
            values["cult_inter_cipan"] is None
            and values["type_cult_eff_prec_id"] is None
        ):
            raise ValidationError(
                "cult_inter_cipan or eff_prec type_cult_eff_prec_id must be provided"
            )
        return values

    @root_validator
    @classmethod
    def check_prairie(cls, values):
        fields = ["age_prairie", "period_destruct", "rang_culture"]
        check_all_or_none(values, fields)
        return values

    @root_validator
    @classmethod
    def check_luzerne(cls, values):
        fields = ["age_luzerne", "rang_luzerne"]
        check_all_or_none(values, fields)
        return values

    @root_validator
    @classmethod
    def check_apport_organique(cls, values):
        fields = [
            "cat_anim",
            "typ_mo",
            "quantite_eff",
            "teneur_n",
            "date_app",
            "cemi_yn",
        ]
        apps = ["app1", "app2", "app3"]
        for app in apps:
            check_all_or_none(
                values,
                [f"{f}_{app}" for f in fields],
            )
        return values

    @root_validator
    @classmethod
    def check_sol(cls, values):
        if (
            values["sol_carbone"] is None
            and values["sol_mo"] is None
            and values["sol_nhumus"] is None
        ):
            raise ValidationError(
                "sol_carbone or sol_mo and sol_nhumus must be provided"
            )
        return values


class RequafertiComputeNResult(BaseModel):
    besoin: Optional[float]  # Besoins totaux en azote (uN) / exaample: 280
    fournitures: Optional[float]  # Fournitures en azote (uN) / example: 225
    fumure: Optional[float]  # Conseil de fumure azotée totale / example: 55
    error: Optional[str]  # Erreurs


class RequafertiGetSolResultType(BaseModel):
    nno3_profil: Optional[float] = Field(description='Profil NNO3 (kg N/ha)')
    nnh4_profil: Optional[float] = Field(description='Profil NNH4 (kg N/ha)')
    cailloux: Optional[float] = Field(description='Charge caillouteuse (%)')
    densite: Optional[float] = Field(description='Densité')
    couche_arable: Optional[float] = Field(description='Profondeur de la couche arable (cm)')
    temp_air: Optional[float] = Field(description="Température de l'air moyenne (°C)")
    argile: Optional[float] = Field(description='Teneur en argile (%)')
    calcaire: Optional[float] = Field(description='Teneur en calcaire (‰)')
    mo: Optional[float] = Field(description='Teneur en matière organique (%)')
    carbone: Optional[float] = Field(description='Teneur en carbone (%)')
    km: Optional[float] = Field(description='Coefficient de minéralisation (Km)')
    fact_conv: Optional[float] = Field(description='Facteur de conversion')
    nhumus: Optional[float] = Field(description='N humus (g/kg ou ‰)')
    reliquat_n: Optional[float] = Field(description='Reliquat (kg/ha)')
    pot_mineral: Optional[float] = Field(description='Potentiel de minéralisation')

    rapport_c_n: Optional[float] = Field(description='Rapport carbone/azote')


class PDFGeneratorInputType(graphene.InputObjectType):
    inputs = graphene.JSONString(required=True)

    class Meta:
        name = "requaferti__pdf_generator_input"


class PDFGeneratorOutputType(graphene.ObjectType):
    pdf = graphene.Base64()

    class Meta:
        name = "requaferti__pdf_generator_output"
