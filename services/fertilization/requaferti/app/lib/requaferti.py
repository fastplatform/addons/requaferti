import logging
import asyncio
from datetime import datetime
import csv

import httpx
import cachetools
from threading import Lock
import fiona
from shapely.geometry import shape
from shapely.strtree import STRtree


from app.settings import config
from app.lib.requaferti_types import (
    RequafertiComputeNParams,
    RequafertiComputeNResult,
    RequafertiGetSolResultType,
)


logger = logging.getLogger(__name__)


class RequafertiClient:
    def __init__(self):
        self.http_client = None

        self.codes_postaux_tree = None
        self.codes_postaux_index = None
        self.regions_agricoles_tree = None
        self.regions_agricoles_index = None
        self.charge_caillouteuse_tree = None
        self.charge_caillouteuse_index = None
        self.parametres_sol_par_code_postal = None
        self.parametres_sol_par_region_agricole = None
        self.parametres_sol_par_defaut = None

        self.configuration_lock = Lock()
        self.configuration = None
        self.configuration_updated_at = None

        self.compute_n_cache = cachetools.TTLCache(
            maxsize=config.REQUAFERTI_COMPUTE_N_CACHE_MAX_SIZE,
            ttl=config.REQUAFERTI_COMPUTE_N_CACHE_TTL,
        )
        self.compute_n_cache_lock = Lock()

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    def load_geographic_data(self):
        """Load and index the GIS layers from shapefiles

        - codes postaux
        - régions agricoles

        """
        logger.info("Loading geomgraphic data in memory")

        logger.info("Loading codes postaux")
        with fiona.open(
            config.APP_DIR / "lib/codes_postaux/codes_postaux.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.codes_postaux_tree = STRtree(shapes)
            self.codes_postaux_index = dict((id(s), f) for s, f in zip(shapes, data))

        logger.info("Loading régions agricoles")
        with fiona.open(
            config.APP_DIR / "lib/regions_agricoles/regions_agricoles.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.regions_agricoles_tree = STRtree(shapes)
            self.regions_agricoles_index = dict(
                (id(s), f) for s, f in zip(shapes, data)
            )

        logger.info("Loading charge caillouteuse")
        with fiona.open(
            config.APP_DIR / "lib/sol/charge_caillouteuse/charge_caillouteuse.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.charge_caillouteuse_tree = STRtree(shapes)
            self.charge_caillouteuse_index = dict(
                (id(s), f) for s, f in zip(shapes, data)
            )

        logger.info("Loading parametres sol par code postal")
        with open(
            config.APP_DIR / "lib/sol/parametres_sol_par_code_postal.csv"
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            self.parametres_sol_par_code_postal = {}
            for row in reader:
                code_postal = row.pop("code_postal")
                self.parametres_sol_par_code_postal[str(code_postal)] = {
                    k: float(v) if v != "" else None for k, v in row.items()
                }

        logger.info("Loading parametres sol par région agricole")
        with open(
            config.APP_DIR / "lib/sol/parametres_sol_par_region_agricole.csv"
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            self.parametres_sol_par_region_agricole = {}
            for row in reader:
                region_agricole = row.pop("region")
                self.parametres_sol_par_region_agricole[region_agricole] = {
                    k: float(v) if v != "" else None for k, v in row.items()
                }

        logger.info("Loading parametres sol par défaut")
        with open(config.APP_DIR / "lib/sol/parametres_sol_par_defaut.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            row = next(reader)
            self.parametres_sol_par_defaut = {
                k: float(v) if v != "" else None for k, v in row.items()
            }

    async def get_config(self):
        """Loads the Requaferti configuration from the `liste` and `dico` Requasud web
        services

        The config is cached in memory for a duration of REQUAFERTI_CONFIG_CACHE_TTL
        seconds
        """

        if self.configuration is not None and (
            (self.configuration_updated_at - datetime.now()).total_seconds()
            <= config.REQUAFERTI_CONFIG_CACHE_TTL
        ):
            logger.debug("Reading config from cache")
            return self.configuration

        logger.info("Refreshing config from the Requaferti endpoint")
        # Lock the configuration for updating
        with self.configuration_lock:

            try:
                liste_dicos = config.REQUAFERTI_CONFIG_LISTE_DICOS.split(",")

                # This semaphore will block the number of downloads under the max
                # concurrency
                semaphore = asyncio.Semaphore(config.REQUAFERTI_CONFIG_DICO_CONCURRENCY)
                lock = Lock()

                configuration = {}

                async def download_dico(dico):
                    logger.info("Refreshing config from the Requaferti endpoint: %s", dico)
                    async with semaphore:
                        response = await self.http_client.post(
                            config.REQUAFERTI_CONFIG_DICO_URL,
                            data={"dico_flag": dico},
                            timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                            auth=(
                                config.REQUAFERTI_USERNAME,
                                config.REQUAFERTI_PASSWORD,
                            ),
                        )
                        response.raise_for_status()

                        data = response.json()["dico"]

                        # Write to the config dict, under a lock to prevent concurrent
                        # writes
                        with lock:
                            configuration[dico] = data

                download_dicos = [download_dico(dico) for dico in liste_dicos]
                await asyncio.gather(*download_dicos)

            except:
                logger.exception("Failed to refresh the configuration")
            else:
                self.configuration = configuration
                self.configuration_updated_at = datetime.now()

        return self.configuration

    def get_code_postal(self, geometry):
        """Return the postal code of a geometry

        Args:
            geometry (dict): a __geo_interface__ dict on EPSG:31370

        This is computed on the basis of the codes_postaux/codes_postaux.shp
        file in the lib directory.

        Returns:
            str: the postal code
        """
        geometry = shape(geometry)
        code_postal_feature = self._get_max_intersection_from_geometry(
            geometry, self.codes_postaux_tree, self.codes_postaux_index
        )
        return (
            code_postal_feature["properties"]["nouveau_PO"]
            if code_postal_feature is not None
            else None
        )

    def get_region_agricole(self, geometry):
        """Return the agricultural region of a geometry

        This is computed on the basis of the regions_agricoles/regions_agricoles.shp
        file in the lib directory.

        Args:
            geometry (dict): a __geo_interface__ dict on EPSG:31370

        Returns:
            str: the name of the agricultural region
        """
        geometry = shape(geometry)
        region_agricole_feature = self._get_max_intersection_from_geometry(
            geometry, self.regions_agricoles_tree, self.regions_agricoles_index
        )
        return (
            region_agricole_feature["properties"]["NOM"]
            if region_agricole_feature is not None
            else None
        )

    def get_charge_caillouteuse(self, geometry):
        geometry = shape(geometry)
        return self._get_avg_field_intersection_from_geometry(
            geometry,
            self.charge_caillouteuse_tree,
            self.charge_caillouteuse_index,
            "CH_EST_SUR",
        )

    def _get_max_intersection_from_geometry(self, geometry, tree, index):
        """Return the feature from a layer that max intersects the provided geometry

        Args:
            geometry (shape): a Shapely geometry object
            tree (STRTree): the index tree as built from the source layer
            index (dict): an index mapping the memory id and a feature, as built from
                            the source layer

        Returns:
            geometry (dict): a __geo_interface__ dict on EPSG:31370
        """
        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in tree.query(geometry)]

        if not shape_ids:
            return None

        # Features corresponding to these memory IDs, as per the index
        features = [index[shape_id] for shape_id in shape_ids]

        # Find max intersection
        intersection_areas = [
            shape(f["geometry"]).intersection(geometry).area for f in features
        ]
        index_intersection_area_max = max(
            range(len(intersection_areas)), key=lambda i: intersection_areas[i]
        )

        return features[index_intersection_area_max]

    def _get_avg_field_intersection_from_geometry(self, geometry, tree, index, field):
        """Return the average value of a field of all the features from a layer that 
        intersect the provided geometry

        Args:
            geometry (shape): a Shapely geometry object
            tree (STRTree): the index tree as built from the source layer
            index (dict): an index mapping the memory id and a feature, as built from
                            the source layer
            field (str): which field to use for the average

        Returns:
            geometry (dict): a __geo_interface__ dict on EPSG:31370
        """
        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in tree.query(geometry)]

        if not shape_ids:
            return None

        # Features corresponding to these memory IDs, as per the index
        features = [index[shape_id] for shape_id in shape_ids]

        # Find weighted averageof the field on the intersecting geometries
        intersection_areas = [
            shape(f["geometry"]).intersection(geometry).area for f in features
        ]
        values = [float(f["properties"].get(field)) for f in features]

        # If all the intersecting geometries have no value, return None
        if all([v is None for v in values]):
            return None

        # If we are querying with a geometry that is a Point, then the intersection
        # areas will be zero for all intersections. In that case, we will just be
        # returning the average of the values.
        if all([i is None or i == 0 for i in intersection_areas]):
            intersection_areas = [1] * len(intersection_areas)

        # Return the average of the values of the field, weighted by the intersection
        # areas
        average = sum(
            [i * v for i, v in zip(intersection_areas, values) if v is not None]
        ) / sum([i for i, v in zip(intersection_areas, values) if v is not None])

        return average

    def get_sol_par_code_postal(self, code_postal):
        return self.parametres_sol_par_code_postal.get(str(code_postal), None)

    def get_sol_par_region_agricole(self, region_agricole):
        return self.parametres_sol_par_region_agricole.get(region_agricole, None)

    def get_sol_par_defaut(self):
        return self.parametres_sol_par_defaut

    def get_sol(self, geometry, **kwargs):

        sol = RequafertiGetSolResultType()

        # First assign default values for whole Wallonia
        for k, v in requaferti_client.get_sol_par_defaut().items():
            if v is not None:
                setattr(sol, k, v)

        # Then override with values from the region agricole
        region_agricole = requaferti_client.get_region_agricole(geometry)
        if region_agricole is not None:
            sol_region_agricole = requaferti_client.get_sol_par_region_agricole(
                region_agricole
            )
            if sol_region_agricole is not None:
                for k, v in sol_region_agricole.items():
                    if v is not None:
                        setattr(sol, k, v)

        # Then override with values from the postal code
        code_postal = requaferti_client.get_code_postal(geometry)
        if code_postal is not None:
            sol_code_postal = requaferti_client.get_sol_par_code_postal(code_postal)
            if sol_code_postal is not None:
                for k, v in sol_code_postal.items():
                    if v is not None:
                        setattr(sol, k, v)

        # Then override with values from the charge_caillouteuse layers
        cailloux = requaferti_client.get_charge_caillouteuse(geometry)
        if cailloux is not None:
            cailloux = round(cailloux, 2)
            setattr(sol, "cailloux", cailloux)

        # Then override with values from the user (passed as arguments)
        for k, v in kwargs.items():
            if v is not None:
                setattr(sol, k, v)

        # Compute automatic coefficients and convert units as necessary
        # for Requaferti
        if sol.couche_arable is not None:
            sol.couche_arable *= 100  # m to cm

        if sol.calcaire is not None:
            sol.calcaire *= 10  # percent to permille

        if sol.temp_air is not None:
            sol.fact_conv = max(0.2 * sol.temp_air - 1, 0)

        if sol.carbone is not None and sol.mo is None:
            sol.mo = 2 * sol.carbone

        if (
            sol.mo is not None
            and sol.fact_conv is not None
            and sol.rapport_c_n is not None
            and sol.rapport_c_n != 0
            and sol.fact_conv != 0
            and sol.nhumus is not None
        ):
            sol.nhumus = sol.mo / (sol.rapport_c_n * sol.fact_conv) * 10

        if (
            sol.fact_conv is not None
            and sol.argile is not None
            and sol.calcaire is not None
        ):
            sol.km = (
                1200 * sol.fact_conv / ((sol.argile + 200) * (sol.calcaire * 0.3 + 200))
            )

        return sol

    async def compute_n(self, params: RequafertiComputeNParams):
        """Computes the N recommendation from Requaferti by caling the Requasud
        webservice

        A max of REQUAFERTI_COMPUTE_N_CACHE_MAX_SIZE results are cached in an TTLCache,
        and returned from there if existing.
        The results in the cache are expired after REQUAFERTI_COMPUTE_N_CACHE_TTL seconds.

        Args:
            params (RequafertiComputeNParams): Input parameters

        Raises:
            GraphQLError: [description]

        Returns:
            RequafertiComputeNResult: Recommendation result
        """

        # Read and return from cache if entry exists
        with self.compute_n_cache_lock:
            result = self.compute_n_cache.get(str(params))

        if result is not None:
            logger.debug("compute_n: reading result from cache")
            return result

        # Otherwise, query from the API
        query_params = params.dict()
        # Convert Boolean params to 0 or 1 expected by the Requaferti API
        for param_name in query_params:
            if isinstance(query_params[param_name], bool):
                query_params[param_name] = 1 if query_params[param_name] else 0

        response = await self.http_client.post(
            config.REQUAFERTI_COMPUTE_N_URL,
            data=query_params,
            timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
            auth=(config.REQUAFERTI_USERNAME, config.REQUAFERTI_PASSWORD),
        )
        response.raise_for_status()

        # Parse the API result
        result = RequafertiComputeNResult(**response.json())

        # Set the value in cache
        with self.compute_n_cache_lock:
            self.compute_n_cache[str(params)] = result

        # Return the result as a pydantic object
        return result


requaferti_client = RequafertiClient()
