# `addons/requaferti`: Requaferti fertilization service for Wallonia

This service exposes [Requaferti](https://www.requaferti.requasud.be/) fertilization recommendation to the Wallonian users of FaST and is based on a web service exposed by [Requasud](https://www.requasud.be/). The service itself does not do any fertilization computation (i.e. it contains no agronomic logic): all the calculations are done in the Requasud/Requaferti web service and this connector only acts as a proxy.

As an additional module of the FaST Platform, it integrates the main service(s) necessary for its operation as well as any other ancillary systems such as databases or cache systems. This repo includes both the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

Services:
* [addons/requaferti/fertilization](services/fertilization/requaferti)
