load("@io_bazel_rules_docker//repositories:repositories.bzl", container_repositories = "repositories")
load("@rules_python//python:repositories.bzl", python_repositories = "py_repositories")
load("@rules_python_external//:repositories.bzl", python_external_dependencies = "rules_python_external_dependencies")

def rules_docker_setup():
    container_repositories()

def rules_python_setup():
    python_repositories()
    python_external_dependencies()

def setup():
    rules_docker_setup()
    rules_python_setup()

