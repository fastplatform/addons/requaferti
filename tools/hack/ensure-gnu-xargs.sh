#!/bin/bash


set -o nounset
set -o pipefail

check_xargs_version() {

  if ! [ -x "$(command -v xargs)" ]; then
    echo "xargs not in path !"
    return 1
  fi

  xargs --version >/dev/null 2>&1
  if ! [ $? -eq "0" ]; then
    echo "Current xargs version is not the GNU one."
    echo "Requires the GNU version of xargs."
    echo "OSX -> brew install findutils && ln -s /usr/local/bin/gxargs /usr/local/bin/xargs"
  fi
}

check_xargs_version